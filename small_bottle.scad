// Small bottle design

include <rounded_edge_cylinder.scad>

module small_bottle() {
	bodyHeight = 100;
	bodyRadius = 15;
	
	bodyWallThickness = bodyRadius - (bodyRadius * 0.95);
	echo (bodyWallThickness);
	
	bottleCapWidth = bodyRadius * 0.7;
	bottleCapHeight = bottleCapWidth * 0.4;
	
	neckHeight = bottleCapHeight * 0.7;
	neckRadius = bottleCapWidth * 0.7 * 0.5;
	
	bottleCapZ = bodyHeight + neckHeight;
	
	union() {
		bottle_body(bodyHeight, bodyRadius, bodyWallThickness);
		% bottle_neck(neckHeight, neckRadius, bottleCapWidth, bodyHeight);
		color("blue") bottle_cap(bottleCapWidth, bottleCapHeight, [0, 0, bottleCapZ]);
	}
	
}


module bottle_body(height, radius, bodyWallThickness) {
	topSphereRadius = radius * 0.4;
	bottomSphereRadius = radius * 0.2;
	
	// adaptedCylinderRadius = radius - (topSphereRadius + bottomSphereRadius);
	// adaptedCylinderHeight = height - (topSphereRadius + bottomSphereRadius);
	
	// translate([0,0,bottomSphereRadius]) {
	// 	minkowski() {
	// 		cylinder(h=adaptedCylinderHeight, r=adaptedCylinderRadius);
	// 		half_sphere(r=bottomSphereRadius);
	// 		rotate(a=[0,180,0]) half_sphere(r=topSphereRadius);
	// 	}
	// }
	
	% difference() {
		rounded_edge_cylinder(h=height, r=radius, topEdge=topSphereRadius, bottomEdge=bottomSphereRadius);
		translate([0,0,(height - (height - bodyWallThickness) ) / 2]) rounded_edge_cylinder(h=height - bodyWallThickness, r=radius - bodyWallThickness, topEdge=topSphereRadius - bodyWallThickness, bottomEdge=bottomSphereRadius - bodyWallThickness);
	}
	
	color([173/255,216/255,230/255, 0.8]) translate([0,0,bodyWallThickness]) bottle_contents(height=(height - bodyWallThickness) * 0.9, radius=radius - bodyWallThickness, bottomSphereRadius=bottomSphereRadius - bodyWallThickness);
	
	% translate(v=[radius-0.2,-5,height-15]) rotate(a=[0,90,0]) text("Water bottle");
	
	// translate([40,0,0]) rounded_edge_cylinder(h=height, r=radius, topEdge=topSphereRadius, bottomEdge=bottomSphereRadius);
}

module bottle_cap(diameter, height, pos) {
	translate(pos) {
		cylinder(h=height, r=(diameter * 0.5), center=false, $fn=20);
	}
}

module bottle_neck(height, radius, capWidth, zPos) {
	ringWidth = capWidth * 1.2;
	ringHeight = 1;
	
	translate([0,0, zPos]) {
		cylinder(h=height, r=radius);
		translate(v=[0,0,height - ringHeight]) cylinder(h=ringHeight, r=(ringWidth * 0.5));
	}
}

module bottle_contents(height, radius, bottomSphereRadius) {
	// translate([0,0,20]) sphere(r=radius);
	
	rounded_edge_cylinder(h=height, r=radius, topEdge=0.01, bottomEdge=bottomSphereRadius);
}


module half_sphere(r=1) {
	difference() {
		sphere(r=r);
		translate([0,0,r*0.5]) cube(size=[r*2, r*2, r], center=true);
	}
}


small_bottle();
