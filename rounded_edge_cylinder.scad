

module rounded_edge_cylinder(h=1, r=1, topEdge=0, bottomEdge=0) {
	// cylinder(h=h, r=r);
	
	topSphereRadius = topEdge;
	bottomSphereRadius = bottomEdge;
	
	adaptedCylinderRadius = r - (topSphereRadius + bottomSphereRadius);
	adaptedCylinderHeight = h - (topSphereRadius + bottomSphereRadius);
	
	translate([0,0,bottomSphereRadius]) {
		minkowski() {
			cylinder(h=adaptedCylinderHeight, r=adaptedCylinderRadius);
			half_sphere(r=bottomSphereRadius);
			rotate(a=[0,180,0]) half_sphere(r=topSphereRadius);
		}
	}
}


module half_sphere(r=1) {
	difference() {
		sphere(r=r);
		translate([0,0,r*0.5]) cube(size=[r*2, r*2, r], center=true);
	}
}

// topSphereRadius = 15 * 0.4;
// bottomSphereRadius = 15 * 0.2;
// rounded_edge_cylinder(h=100, r=15, topEdge=topSphereRadius, bottomEdge=bottomSphereRadius);